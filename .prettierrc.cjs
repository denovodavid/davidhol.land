module.exports = {
    printWidth: 100,
    tabWidth: 4,
    useTabs: false,
    semi: true,
    singleQuote: true,
    trailingComma: 'all',
    bracketSpacing: true,
    plugins: [require('prettier-plugin-packagejson'), require('prettier-plugin-astro')],
    overrides: [
        {
            files: '*.astro',
            options: { parser: 'astro' },
        },
    ],
};
