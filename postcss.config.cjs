/** @type {import('postcss-load-config')} */
module.exports = () => ({
    plugins: [
        require('postcss-flexbugs-fixes')(),
        require('postcss-preset-env')({
            stage: 0,
            autoprefixer: { flexbox: 'no-2009' },
        }),
    ],
});
