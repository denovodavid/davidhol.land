import {
    defineConfig,
    presetIcons,
    presetTypography,
    presetWebFonts,
    presetWind,
    transformerDirectives,
    transformerVariantGroup,
} from 'unocss';

export default defineConfig({
    presets: [
        presetIcons({
            extraProperties: {
                display: 'inline-block',
                height: '1.2em',
                width: '1.2em',
                'vertical-align': 'text-bottom',
            },
        }),
        presetWind(),
        presetTypography(),
        presetWebFonts({
            fonts: {
                sans: 'Rubik',
                // serif: 'Lora',
                // mono: 'Overpass Mono',
                // display: 'Yeseva One',
            },
        }),
    ],
    transformers: [transformerDirectives(), transformerVariantGroup()],
    shortcuts: {
        'text-subtle': 'text-gray-500 dark:text-gray-500',
        'text-subtler': 'text-gray-400 dark:text-gray-600',
    },
});
