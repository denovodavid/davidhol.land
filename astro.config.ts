import mdx from '@astrojs/mdx';
import sitemap from '@astrojs/sitemap';
import { defineConfig } from 'astro/config';
import { dirname, join } from 'node:path';
import UnoCSS from 'unocss/astro';
import { fileURLToPath } from 'node:url';
import child_process from 'node:child_process';

const __dirname = dirname(fileURLToPath(import.meta.url));

async function run(command: string) {
    return new Promise<{ stdout: string; stderr: string }>((resolve, reject) => {
        child_process.exec(command, (err, stdout, stderr) => {
            if (err && err.code !== 0) {
                reject(new Error(err.message));
            } else {
                resolve({ stdout, stderr });
            }
        });
    });
}

if (!process.env.CI) {
    process.env.BRANCH = (await run('git branch --show-current')).stdout.trim();
}

// https://astro.build/config
export default defineConfig({
    site: 'https://www.davidhol.land',
    integrations: [mdx(), sitemap(), UnoCSS()],
    markdown: {
        shikiConfig: {
            theme: 'github-dark-dimmed',
            langs: [
                {
                    id: 'zig',
                    scopeName: 'source.zig',
                    path: join(__dirname, 'src/zig.tmLanguage.json'),
                },
            ],
        },
    },
});
