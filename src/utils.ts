export function dateFormat(date: Date) {
    return date.toLocaleDateString('en-GB').replace(/\//g, '.');
}
