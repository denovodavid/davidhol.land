import rss from '@astrojs/rss';
import { APIRoute } from 'astro';
import MarkdownIt from 'markdown-it';
import sanitizeHtml from 'sanitize-html';
import { SITE_DESCRIPTION, SITE_TITLE } from '../config';
import { getCollectionArticles } from '../content/_collections';

const mdParser = new MarkdownIt();

export const get: APIRoute = async (context) => {
    const articles = (await getCollectionArticles()).sort(
        (a, b) => b.data.published.valueOf() - a.data.published.valueOf(),
    );
    return rss({
        title: SITE_TITLE,
        description: SITE_DESCRIPTION,
        site: context.site!.href,
        items: articles.map((post) => ({
            title: post.data.title,
            pubDate: post.data.published,
            description: post.data.description,
            content: sanitizeHtml(mdParser.render(post.body)),
            link: `/articles/${post.slug}/`,
        })),
    });
};
