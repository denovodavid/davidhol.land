import { getCollection } from 'astro:content';

const INCLUDE_DRAFTS = import.meta.env.DEV || import.meta.env.BRANCH !== 'master';

export function getCollectionArticles() {
    return getCollection('articles', ({ data }) => INCLUDE_DRAFTS || !data.draft);
}
