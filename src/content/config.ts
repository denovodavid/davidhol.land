import { defineCollection, z } from 'astro:content';

export const collections = {
    articles: defineCollection({
        schema: z.object({
            draft: z.boolean(),
            title: z.string(),
            description: z.string(),
            published: z.coerce.date(),
            tags: z.array(z.string()),
        }),
    }),
};
