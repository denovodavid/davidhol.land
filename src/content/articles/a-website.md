---
draft: false
title: A website
description: So, you finally made your own website.
published: 2022-12-15
tags: []
---

This is indeed a website.

I just wanted somewhere to write an article that was my own. And, eventually I will do that. This is simply the first flag in the titular ground that is _The Web_.

Bye.
